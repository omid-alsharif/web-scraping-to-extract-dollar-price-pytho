#!/usr/bin/env python3
import requests
import bs4
import re

page = requests.get("http://www.tgju.org/currency")
soup = bs4.BeautifulSoup(page.content , "lxml")
#print(soup.prettify())

dol = soup.findAll("td")

dollar = str(dol[0])
final = re.sub('[^0-9]','', dollar)

print(final)
